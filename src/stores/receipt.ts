import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

import type { Product } from '@/types/Product'
import type { ReceiptItem } from '@/types/ReceiptItem'

export const useReceiptStore = defineStore('counter', () => {
  const receiptItems = ref<ReceiptItem[]>([])
  function addReceiptItem(product: Product) {
    const index = receiptItems.value.findIndex((item) => item.product?.id === product.id)
    if (index >= 0) {
      receiptItems.value[index].unit++
    } else {
      const newReceipt: ReceiptItem = {
        id: -1,
        name: product.name,
        price: product.price,
        unit: 1,
        productId: product.id,
        product: product
      }
      receiptItems.value.push(newReceipt)
    }
  }

  function removeReceiptItem(receiptItem: ReceiptItem) {
    const index = receiptItems.value.findIndex((item) => item === receiptItem)
    receiptItems.value.splice(index, 1)
  }

  function inc(item: ReceiptItem) {
    item.unit++
  }
  function dec(item: ReceiptItem) {
    if (item.unit === 1) {
      removeReceiptItem(item)
    }
    item.unit--
  }

  return {
    receiptItems,
    addReceiptItem,removeReceiptItem,inc,dec}
})
